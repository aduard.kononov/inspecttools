# Functions
* Getting of the way the programmer calls a function,
i.e. the exact string that calls the function
```python
from inspecttools import get_invocation_way
def f(*args, **kwargs):
    print(get_invocation_way())
f(1, 2, 3, 5, 6, 7, 8, 7, z=4, t=9)
```

* The name of the function explains its behaviour well enough. 
The code will not raise any exception and the function 
will be called as if everything is fine: 
extra args and unexpected kwargs will be ignored.
```python
from inspecttools import suppress_unexpected_args
@suppress_unexpected_args
def f(x, y, z):
    print(x, y, z)

f(1, 2, 3, 4, 5, 6, t=9, sad=True)
```