from inspecttools.extended_types import FunctionType

import inspect
from inspect import Parameter
from functools import wraps
from itertools import takewhile, chain

__all__ = ['suppress_unexpected_args']


# TODO(ekon): handle this:
#  TypeError: <func_name>() got multiple values for argument <attr_name>
def suppress_unexpected_args(func: FunctionType):
    signature = inspect.signature(func)
    params = signature.parameters.values()
    positional_or_keyword = []
    kwarg_only = []
    args_counter = {
        Parameter.POSITIONAL_ONLY: 0,
        Parameter.POSITIONAL_OR_KEYWORD: 0,
        Parameter.VAR_POSITIONAL: 0,
        Parameter.KEYWORD_ONLY: 0,
        Parameter.VAR_KEYWORD: 0,
    }

    param: Parameter
    for param in params:
        name = param.name
        kind = param.kind

        args_counter[kind] += 1

        if kind is Parameter.KEYWORD_ONLY:
            kwarg_only.append(name)
        elif kind is Parameter.POSITIONAL_OR_KEYWORD:
            positional_or_keyword.append(name)

    @wraps(func)
    def inner(*args, **kwargs):
        safe_args = []
        safe_kwargs = {}

        if args_counter[Parameter.VAR_POSITIONAL]:
            safe_args = args
        else:
            positional_total = args_counter[Parameter.POSITIONAL_ONLY] + args_counter[Parameter.POSITIONAL_OR_KEYWORD]
            for arg in takewhile(lambda x: len(safe_args) < positional_total, args):
                safe_args.append(arg)

        if args_counter[Parameter.VAR_KEYWORD]:
            safe_kwargs = kwargs
        else:
            for arg_name, arg_value in kwargs.items():
                if arg_name in chain(positional_or_keyword, kwarg_only):
                    safe_kwargs[arg_name] = arg_value

        result = func(*safe_args, **safe_kwargs)
        return result

    return inner


def main():
    @suppress_unexpected_args
    def f(x, y, z):
        print(x, y, z)

    f(1, 2, 3, 4, 5, 6, t=9, sad=True)


if __name__ == '__main__':
    main()
